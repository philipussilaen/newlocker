<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ExpressPartners;
use Illuminate\Support\Facades\DB;

class LazadaMYPushOverdue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada-my:overdue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Overdue dan Push to partners';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $partners = DB::table('tb_express_partners')
                    ->leftjoin('tb_newlocker_express', 'tb_newlocker_express.id', '=', 'tb_express_partners.id_express')
                    ->where('last_status', 'IN_STORE')
                    ->where('logistics_company', '402880825c2ddbf5015c42c61eeb2647')
                    ->get();
        echo "BEGIN CHECKING OVERDUE";
        if ($partners) {
            foreach ($partners as $key => $value) {
                if($value->overdueTime / 1000 < time()) {
                    $expressPartners = new ExpressPartners;
                    $push = $expressPartners->updateData($value->id, 'OVERDUE');
                    echo "push overdue $value->expressNumber -> $push";
                }
            }
        } else {
            echo "No Overdue Data";
        }
    }
}
