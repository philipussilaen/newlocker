<?php

namespace App\Console\Commands;

use App\Models\SMSLog;
use App\Models\Express;
use App\Http\Helpers\Helper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BlastResendSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blastsms:resend';
    private $response_= null;
    private $originURL_= null;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Blast RESEND sms status Failed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_date = '2019-08-08 00:00:01';
        $end_date = '2019-08-08 23:59:59';
        $SMSLogDB = new SMSLog;
        $dataSMSLog = $SMSLogDB->getParcelHasFailedSMS($start_date, $end_date);
        
        foreach ($dataSMSLog as $key => $value) {
            if (isset($value)) {
                $sms_content = preg_replace('/[[\s\S]+?]/', '', $value->sms_content);
                $express = $this->getExpress($value->express_id);
                $operator_id = '';
                $take_phone_number = '';
                $express_num = '';
                if ($express)  {
                    $operator_id = $express->operator_id;
                    $take_phone_number = $express->takeUserPhoneNumber;
                    $express_num = $express->expressNumber;
                }
                $this->sendSMS($value->express_id, $operator_id, $take_phone_number, $sms_content);
                echo "$express_num -> SMS send to ".$take_phone_number ."\n";
            } else {
                echo "DATA IS NULL";
            }
        }
    }

    public function getExpress($express_id)
    {
        $expressDB = new Express;
        $dataExpress = $expressDB->getExpress($express_id);
        return $dataExpress;
    }

    public function sendSMS($id, $operator_id, $takeUserPhoneNumber, $message)
    {
        if($operator_id == '145b2728140f11e5bdbd0242ac110001' || substr($takeUserPhoneNumber, 0, 2) == '08'){
            // $resp = $this->sendFromNexmo($takeUserPhoneNumber, $message);
        }else{
            $resp = $this->sendFromIsentric($takeUserPhoneNumber, $message);
        }
        if (!empty($resp)){
            $statusMsg = (strpos($resp, "0") != false) ? 'SUCCESS' : 'FAILED' ;
        } else {
            $statusMsg = 'ERROR';
        }
        DB::table('tb_newlocker_smslog')
            ->insert([
                'express_id' => $id,
                'sms_content' => '[Reminder] '.$message,
                'sms_status' => $statusMsg,
                'sent_on' => date("Y-m-d H:i:s"),
                'original_response' => $this->response_
        ]);
        Helper::LogPayment($id. ' - '.$takeUserPhoneNumber."-".$statusMsg.' \n', 'sms-blast-resend', 'log-resend.'.date("Y-m-d"));
    }

    public function sendFromNexmo($to, $message){        
        // $notif      = $this->_checkToken($req->json('token'));
        $response = null;
        if(!empty($to) && !empty($message)) {
            if(substr($to, 0, 1) == '+') {
                $to = substr($to, 1);
            } else if(substr($to, 0, 1) == '0') {
                $to = '62'.substr($to, 1);
            } else if(substr($to, 0, 1) == '8') {
                $to = '62'.$to;
            } else if(substr($to, 0, 2) == '01') { //Can also handle Malaysia Phone Number. [Wahyudi 09-09-17] for Malaysia Backup
                $to = '6'.$to;
            }
            $data = array();
            $data["api_key"] = '5b582569';
            $data["api_secret"] = 'f1708d28f0dfaffa';
            $data["from"] = 'POPBOX-ASIA';
            $data["to"] = $to;
            $data["text"] = $message;
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query($data);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $get = curl_exec($ch);
            $status = json_decode($get, true);
            $response = '"status":"'.$status['messages'][0]['status'].'"';
            DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' => $url,
                    'api_send_data' => json_encode($data),
                    'api_response' => $response,
                    'response_date' => date("Y-m-d H:i:s")
                ]);
        }
        return $response;
    }

    public function sendFromIsentric ($to, $message){
        $response = '';
        if(!empty($to) && !empty($message)) {
            if(substr($to, 0, 1) == '+') {
                $to = substr($to, 1);
            } else if(substr($to, 0, 1) == '0') {
                $to = '60'.substr($to, 1);
            } else if(substr($to, 0, 1) == '1') {
                $to = '60'.$to;
            }
            $message = urlencode($message);
            $mtid = "707".time().rand(100, 999);
            $accountName = 'popboxsunway'; //This is live account, please don't change..! [Wahyudi 09-09-17]
            // $server_ip = '203.223.130.118';
            $server_ip = '203.223.130.115';
            $runfile = 'http://'.$server_ip.'/ExtMTPush/extmtpush?shortcode=39398&custid='.$accountName.'&rmsisdn='.$to.'&smsisdn=62003&mtid='.$mtid.'&mtprice=000&productCode=&productType=4&keyword=&dataEncoding=0&dataStr='.$message.'&dataUrl=&dnRep=0&groupTag=10';
            $this->url_get = $runfile;
            $this->originURL_ = $runfile;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $runfile);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            
            $content = curl_exec ($ch);
            $ret = strpos($content, "returnCode = ");
            $start = $ret + 13;
            $retcode = substr($content, $start, 1);
            curl_close ($ch);
            $this->response_ = $content;
            $response = '"status":"'.$retcode.'"';
            DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' => 'http://'.$server_ip.'/ExtMTPush/extmtpush?shortcode=39398',
                    'api_send_data' => $runfile,
                    'api_response' => $content,
                    'response_date' => date("Y-m-d H:i:s")
                ]);
        }
        return $response;
    }
}
