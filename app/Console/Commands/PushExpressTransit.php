<?php

namespace App\Console\Commands;

use App\Http\Helpers\Helper;
use Illuminate\Console\Command;
use App\Models\PopExpressTransit;

class PushExpressTransit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:express-transit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Data Express Transit while data transferred yet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "BEGIN PUSH DATA \n";
        $pushData = PopExpressTransit::where('is_transit', 0)->get();
        if ($pushData) {
            foreach ($pushData as $key => $push) { 
                $expressTransit = new PopExpressTransit;
                $param = (array)json_decode($push->param_request);
                $transit = $expressTransit->transit($param);
                if ($transit) {
                    echo ">> ".$param['expressNumber'] ." - ". $transit->message."\n";
                    Helper::LogPayment($param['expressNumber']. ' - '.json_encode($transit).' \n', 'express-transit-log', 'cron-log.'.date("Y-m-d"));
                }
            }
        }
        echo "PUSH DATA FINISH \n";
    }
}
