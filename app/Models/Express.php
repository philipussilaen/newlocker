<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Express extends Model
{
    protected $table = 'tb_newlocker_express';

    public function expressChecking(Request $req, $imported)
    {
     
        // MERCHANT RETURN
        $merchant = new MerchantReturn;
        $merchant_list = $merchant->getMerchant();

        // cek di tabel express
        $sql = "select * from tb_newlocker_express where deleteFlag = '0' and customerStoreNumber = '".$imported."' and expressType = 'CUSTOMER_STORE' and status = 'IMPORTED'";
        $exp = DB::select($sql);
        
        if (count($exp) != 0 ) {
            // ambil data logistic company
            $logisticsCompany_id = $exp[0]->logisticsCompany_id;

            if(empty($logisticsCompany_id)){
                $logisticsCompany_id  = '161e5ed1140f11e5bdbd0242ac110001';
            }

            $sqlc = "select * from tb_newlocker_company where id_company='".$logisticsCompany_id."'";
            $rc = DB::select($sqlc);  

            if (!empty($rc)) {
                // ambil data parent company
                $sqlp = "select * from tb_newlocker_company where id_company='".$rc[0]->id_parent."'";
                $rp = DB::select($sqlp);
            } else {
                $rp = "";
            }
            
            if (!empty($rp) ) {
                // ambil data parent company level grand
                $sqlk = "select * from tb_newlocker_company where id_company='".$rp[0]->id_parent."'";
                $rk = DB::select($sqlk);
            } else {    
                $rk = "";
            }

            if (!empty($rk)) {
                $grandcompany =  array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rk[0]->company_name, 'contactEmail' => [], 'id' => $rk[0]->id_company, 'level' => $rk[0]->level, 'companyType' => $rk[0]->company_type );
            } else {
                $grandcompany = "";
            }

            if (!empty($rp)  ) {
                $parentcompany = array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rp[0]->company_name, 'parentCompany' => $grandcompany, 'contactEmail' => [], 'id' => $rp[0]->id_company, 'level' => $rp[0]->level, 'companyType' => $rp[0]->company_type );
            } else {
                $parentcompany = "";

            }
            $logistic = array('contactPhoneNumber' => [], 'deleteFlag' => 0, 'name' => $rc[0]->company_name, 'parentCompany' => $parentcompany , 'contactEmail' => [], 'id' => $rc[0]->id_company, 'level'=> $rc[0]->level, 'companyType' => $rc[0]->company_type );

            if ($exp[0]->groupName == 'POPDEPOSIT') {
                $parcelType = 'POPSAFE';
            } elseif (in_array($exp[0]->groupName, $merchant_list)) {
                $parcelType = 'ONDEMAND';
            } else {
                $parcelType = $exp[0]->groupName;
            }

            $res =  ['endAddress' => $exp[0]->endAddress , 'createTime' => $exp[0]->importTime, 'additionalPayment' => [] , 'expressType' => $exp[0]->expressType, 'status' => $exp[0]->status,  'recipientUserPhoneNumber' => $exp[0]->recipientUserPhoneNumber, 'logisticsCompany' => $logistic, 'customerStoreNumber' => $exp[0]->customerStoreNumber, 'id' => $exp[0]->id, 'barcode' => ['id' => $exp[0]->barcode_id] , 'items' => [], 'includedNumbers' => 0, 'chargeType' => 'NOT_CHARGE', 'recipientName' => $exp[0]->recipientName , 'takeUserPhoneNumber' => $exp[0]->takeUserPhoneNumber, 'parcelType' => $parcelType, 'groupName' => $exp[0]->groupName];

            if(!empty($exp[0]->designationSize)){
                $res['designationSize'] = $exp[0]->designationSize;
            }

        } else {
            $res =  ['statusCode' => 404 , 'errorMessage' => 'express not found'];
        }

        return $res;
    }

    public function getExpress($expres_id)
    {
        $data = Express::where('id', $expres_id)->first();
        return $data;
    }
}
