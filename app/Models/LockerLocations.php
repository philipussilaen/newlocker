<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LockerLocations extends Model
{
    protected $connection = 'popbox';
    protected $table = 'locker_locations';
}
