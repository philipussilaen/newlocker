<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SMSLog extends Model
{
    protected $table = 'tb_newlocker_smslog';

    public function getParcelHasFailedSMS($start_date, $end_date)
    {
        $data = SMSLog::where('sms_status', 'FAILED')
                ->whereBetween('sent_on', [$start_date, $end_date])
                ->get();
        return $data;
    }
}
