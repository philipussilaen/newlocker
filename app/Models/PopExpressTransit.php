<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\PopExpressAPI;
use App\Http\Helpers\Helper;

class PopExpressTransit extends Model
{
    protected $table = 'tb_express_transit';

    public function transit($data)
    {
        $checkData = PopExpressTransit::where('parcel_id', $data['parcel_id'])->first();
        if (!$checkData) {
            try {
                DB::table('tb_express_transit')->insert([
                    "parcel_id" => $data['parcel_id'], 
                    "express_number" => $data['expressNumber'],
                    "param_request" => json_encode($data),
                    "is_transit" => 0
                ]);
            } catch (\Exception $e) {
                Helper::LogPayment($data['expressNumber']. ' - '.$e->getMessage().' \n', 'express-transit-log', 'error-log.'.date("Y-m-d"));
            }
        }
        
        $popExpressTransit = new PopExpressAPI;            
        $result = $popExpressTransit->sendCallback($data);
        if ($result->code == "200") {
            try {
                DB::table('tb_express_transit')->where('parcel_id', $data['parcel_id'])->update([
                    "is_transit" => 1
                ]);
            } catch (\Exception $e) {
                Helper::LogPayment($data['expressNumber']. ' - '.$e->getMessage().' \n', 'express-transit-log', 'error-log.'.date("Y-m-d"));
            }
        }

        return $result;
    }
}
