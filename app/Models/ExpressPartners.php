<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Http\Helpers\WebCurl;
use App\Http\Helpers\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ExpressPartners extends Model
{
    protected $table = 'tb_express_partners';

    public function add($request, $status)
    {
        $id_express = $request->input('id');
        $logistics_company = $request->input('logisticCompany');
        $express_number = $request->input('expressNumber');

        DB::beginTransaction();
        try {
            $expressPartner = new ExpressPartners;
            $expressPartner->id_express = $id_express;
            $expressPartner->express_number = $express_number;
            $expressPartner->logistics_company = $logistics_company;
            $expressPartner->last_status = $status;
            $expressPartner->save();
            DB::commit();
            return $expressPartner;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function updateData($id_express, $status)
    {
        $data = ExpressPartners::where('id_express', $id_express)->first();
        if ($data) {
            $data->last_status = $status;
            $data->save();
            $send_inbound = $this->send_callback($data->express_number, 'expressNumber');
            if (isset($send_inbound)) {
                if ($send_inbound == "Accepted"){
                    $partners = ExpressPartners::where('id_express', $id_express)->first();
                    if ($status == 'IN_STORE') {
                        $partners->is_inbound_sent = 1;
                        $partners->save();
                    } else if ($status == 'CUSTOMER_TAKEN') {
                        $partners->is_outbound_sent = 1;
                        $partners->save();
                    } else {
                        $partners->is_overdue_sent = 1;
                        $partners->save();
                    }
                    return true;
                } else {
                    Helper::LogPayment($data->express_number. ' - '.$send_inbound.' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                    return false;
                }
            } else {
                $partners = ExpressPartners::where('id_express', $id_express)->first();
                if ($status == 'IN_STORE') {
                    $partners->is_inbound_sent = 1;
                    $partners->save();
                } else if ($status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN') {
                    $partners->is_outbound_sent = 1;
                    $partners->save();
                } else {
                    $partners->is_overdue_sent = 1;
                    $partners->save();
                }
                Helper::LogPayment($data->express_number. ' - '.($send_inbound).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
                return true;
            }
        } else {
            $data = ['message' => 'Data Not Found =>'.$id_express];
            Helper::LogPayment($id_express. ' - '.json_encode($data).' \n', 'callback-log', 'callback-log.'.date("Y-m-d"));
            return json_encode($data);
        }
    }

    public function send_callback($order_number, $column)
    {
        $data_parcel = DB::table('tb_newlocker_express')
                                ->select('tb_newlocker_express.id as id_express', 'tb_newlocker_express.*','tb_detail_express.*','tb_newlocker_box.*')
                                ->leftJoin('tb_newlocker_box', 'tb_newlocker_box.id', '=', 'tb_newlocker_express.box_id')
                                ->leftJoin('tb_detail_express', 'tb_detail_express.parcel_id', '=', 'tb_newlocker_express.id')
                                ->where($column, $order_number)
                                ->first();
        
        if ($data_parcel) {
            $url_tracking = "https://popbox.asia/tracking/" . $data_parcel->expressNumber;
            $output_file_name = $data_parcel->expressNumber."-".$data_parcel->validateCode.".jpg";
            $url_signature = "https://pr0x-my.popbox.asia".'/img/signature/'.$output_file_name; // signature path on server
    
            if ($data_parcel->status == 'IN_STORE') {
                $date_time = date('c', ($data_parcel->storeTime / 1000));
            } else {
                $date_time = date('c', ($data_parcel->takeTime / 1000));
            }
            $param_update = [
                "order_number" => $data_parcel->expressNumber, 
                "tracking_number" => $data_parcel->expressNumber, 
                "status" => $data_parcel->status,
                "date_time" => $date_time,
                "locker_location" => "PopBox @ ".$data_parcel->name, 
                "locker_geolocation" => "", 
                "reason_code" => "", 
                "receiver_name" => (!empty($data_parcel->receive_name)) ? $data_parcel->receive_name : $data_parcel->takeUserPhoneNumber,
                "receiver_id" => (!empty($data_parcel->no_identification)) ? $data_parcel->no_identification : "", 
                "receiver_signature" => $url_signature, 
                "tracking_url" => $url_tracking
            ];
            
            $company = $this->set_company($data_parcel->logisticsCompany_id);
            if ($company) {
                if ($company->id_company == '402880825c2ddbf5015c42c61eeb2647') {
                    $token_lazadamy = env('LEL_TOKEN');
                    $url_lazadamy_prod = env('LEL_URL_CALLBACK') . $token_lazadamy; //production
                    $url_lazadamy_dev = env('LEL_WEBHOOK_TEST'); //testing webhook

                    if (env('APP_ENV') == 'production') {
                        $url_callback = $url_lazadamy_prod;
                    } else {
                        $url_callback = $url_lazadamy_dev;
                    }
                    
                    $curlHelper = new WebCurl;
                    $curl = $curlHelper->post($url_callback, $param_update);
                    DB::table('tb_newlocker_generallog')->insert(
                        ['api_url' =>  $url_callback, 
                        'api_send_data' => json_encode($param_update),
                        'api_response' => json_encode($curl),
                        'response_date' => date("Y-m-d H:i:s")]); 
                    return $curl;
                } else {
                    return "Company Not Found";    
                }
            } else {
                return "Company Not Found";
            }
        } else {
            return "Data Not Found";
        }
    }

    private function set_company($company_id)
    {
        $companyDb = DB::table('tb_newlocker_company')->where('id_company', $company_id)->first();
        if ($companyDb) {
            return $companyDb;
        }
        return false;
    }
}
