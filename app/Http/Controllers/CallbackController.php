<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\WebCurl;
use Illuminate\Support\Facades\DB;
use App\Models\ExpressPartners;

class CallbackController extends Controller
{
    public function resendCallback(Request $request)
    {
        $order_number = $request->input('order_number');
        $column = (!empty($request->input('column'))) ? $request->input('column') : 'expressNumber';
        $expressParners = new ExpressPartners;
        if ($order_number) {
            if (is_array($order_number)) {
                foreach ($order_number as $key => $express_number) {
                    $data = $expressParners->send_callback($express_number, $column);
                    $response[] = [
                        "order_number" => $express_number,
                        "response" => json_decode($data, true)
                    ];
                }
                return $response;
            } else {
                return $expressParners->send_callback($order_number);
            }
        }
    }  
}
