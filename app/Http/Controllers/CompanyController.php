<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;
use App\Http\Helpers\UUIDHelper;
use App\Models\CompanyType;

use App\Http\Requests;

class CompanyController extends Controller
{
    public function create(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        try {
            $response->data = [];
            $company_name = $request->input('company_name');
            $parent_company = !empty($request->input('parent_company')) ? $request->input('parent_company') : '145b2728140f11e5bdbd0242ac110001';
            $level = $request->input('level');
            $company_type = $request->input('company_type');    

            $companyModel = new CompanyType;
            $company = $companyModel->getCompanyById($company_type);

            $type_company = '';
            
            if ($company) {
                $type_company = $company->company_name;
            }
            $id_company = UUIDHelper::generateID();
            $insert = DB::table('tb_newlocker_company')->insert([
                'id_company' => $id_company,
                'company_name' => $company_name,
                'id_parent' => $parent_company,
                'company_type' => $type_company,
                'level' => $level
            ]);

            if ($insert) {
                $response->isSuccess = true;
                $data = DB::table('tb_newlocker_company')->where('id_company', $id_company)->first();
                $response->data = $data;
                return response()->json($response);
            }
            return response()->json($response);            
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return response()->json($response);
        }
    }

    public function getCompany($id_company)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        
        try {
            $response->isSuccess = true;
            $data = DB::table('tb_newlocker_company')->where('id_company', $id_company)->first();
            $response->data = $data;
            return response()->json($response);
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return response()->json($response);
        }
    }

    public function update(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        try {
            $response->data = [];
            $company_name = $request->json('company_name');
            $parent_company = $request->json('parent_company');   
            $company_type = $request->json('company_type');    
            $id_company = $request->json('id_company');    

            $type_company = $this->getCompanyType($company_type);
            $update = DB::table('tb_newlocker_company')
                    ->where('id_company', $id_company)
                    ->update([
                        'company_name' => $company_name,
                        'id_parent' => $parent_company,
                        'company_type' => $type_company,
                    ]);
            
            if ($update) {
                $response->isSuccess = true;
                $data = DB::table('tb_newlocker_company')->where('id_company', $id_company)->first();
                $response->data = $data;
                return response()->json($response);
            } elseif ($update == 0) {
                $response->isSuccess = true;
                $response->message = "Nothing to change";
                return response()->json($response);
            }

            $response->message = "Request format unknown";
            return response()->json($response);
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return response()->json($response);
        }
    }

    public function getCompanyType($id_type)
    {
        $companyModel = new CompanyType;
        $company = $companyModel->getCompanyById($id_type);

        $type_company = '';
        
        if ($company) {
            $type_company = $company->company_name;
        }

        return $type_company;
    }

    public function companyType(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = [];
        try {
            $response->isSuccess = true;
            $data = DB::table('tb_master_company_type')->get();
            $response->data = $data;
            return response()->json($response);
        } catch (\Exception $e) {
            $response->message = $e->getMessage();
            return response()->json($response);
        }
    }

    public function getAllChildCompany(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = [];
        $companyDB = DB::table('tb_newlocker_company')->where('id_company', $request->id_company)->where('deleteFlag', '0')->get();
        $parent = [];

        foreach ($companyDB as $key => $par) {
            $childDb = DB::table('tb_newlocker_company')->where('id_parent', $par->id_company)->where('deleteFlag', '0')->get();
            $childs = [];
            if ($childDb) {
                foreach ($childDb as $key => $child) {
                    $childs[] = (array)$child;
                }
            }
            $parent[] = [
                'id_company' => $par->id_company,
                'company_name' => $par->company_name,
                'company_type' => $par->company_type,
                'company_address' => $par->company_address,
                'id_parent' => $par->id_parent,
                'level' => $par->level,
                'child' => $childs
            ];
        }
        
        if ($parent) {
            $response->isSuccess = true;
            $response->data = $parent;
            return response()->json($response);
        }
        return response()->json();
    }

    public function companyList(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = [];
        $companyDB = DB::table('tb_newlocker_company')->where('deleteFlag', '0')->get();
        $parent = [];

        foreach ($companyDB as $key => $par) {
            $parent[] = $par;
        }
        
        if ($parent) {
            $response->isSuccess = true;
            $response->data = $parent;
            return response()->json($response);
        }
        return response()->json();
    }

    public function createApiKey(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $service_name = $request->input('service_name');
        $header = $request->header('SERVICE-NAME');
        if ($header == 'NUS-POPBOX-90') {
            if (isset($service_name)) {
                $service_key = Helper::generateRandomString(15);
                $popbox_header_auth = Helper::generateRandomString(40);
                
                try {
                    DB::table('tb_client_auth_api')->insert([
                        'service_name' => $service_name,
                        'service_key' => $service_key,
                        'popbox_header_auth' => $popbox_header_auth
                    ]);
                    $response->isSuccess = true;
                    $response->data = [
                        [
                            'service_name' => $service_name,
                            'service_key' => $service_key,
                            'popbox_header_auth' => $popbox_header_auth
                        ]
                    ];
                    return json_encode($response);
                } catch (\Exception $e) {
                    $response->message = $e->getMessage();
                    return response()->json($response);
                }
            }
        } else {
            $response->message = "SERVICE UNKNOWN";
            return response()->json($response);
        }
    }
}
