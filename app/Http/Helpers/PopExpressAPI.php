<?php
namespace App\Http\Helpers;

use Illuminate\Support\Facades\DB;

class PopExpressAPI
{
    private $url;

    public function __construct()
    {
        $this->url = env("POPEXPREXS_TRANSIT_URL");
    }

    public function sendCallback($data)
    {
        $url = $this->url."/api/popbox/transit_locker";
        $curl = new WebCurl;
        $response = $curl->post($url, $data);
        
        DB::table('tb_newlocker_generallog')->insert(
            ['api_url' =>  $url, 
            'api_send_data' => json_encode($data),
            'api_response' => $response,
            'response_date' => date("Y-m-d H:i:s")]); 

        return json_decode($response);
    }

}
?>