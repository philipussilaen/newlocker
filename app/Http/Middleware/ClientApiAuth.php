<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ClientApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $popbox_header_auth = $request->header('POPBOX-HEADER-AUTH');
        $service_key = $request->header('SERVICE-KEY');
        $response = new \stdClass();
        $response->isSuccess = false;

        if (isset($popbox_header_auth) && isset($service_key)) {
            $checking = DB::table('tb_client_auth_api')->where('service_key', $service_key)->where('popbox_header_auth', $popbox_header_auth)->where('is_active', '1')->first();
            
            if ($checking) {
                return $next($request);
            } else {
                $response->message = "Unauthorized";
                return response()->json();
            }
        } else {
            $response->message = "Unauthorized";
            return response()->json();
        }
    }
}
