<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableBlockExpress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_express_blocked'))) {
            Schema::create('tb_express_blocked', function (Blueprint $table) {
                $table->increments('id');
                $table->string('parcel_id');
                $table->string('old_validate_code');
                $table->tinyInteger('is_blocking');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
