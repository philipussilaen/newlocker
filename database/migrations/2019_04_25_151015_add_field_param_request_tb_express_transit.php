<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldParamRequestTbExpressTransit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('tb_express_transit', 'param_request')))
        {
            Schema::table('tb_express_transit', function (Blueprint $table) {
                $table->string('param_request')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_express_transit', function (Blueprint $table) {
            $table->string('param_request')->nullable();           
        });
    }
}
