<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRegisteredOtherCompanyTbNewlockerUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('tb_newlocker_user', 'registered_other_company')))
        {
            Schema::table('tb_newlocker_user', function (Blueprint $table) {
                $table->string('registered_other_company')->nullable()->after('id_company');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
