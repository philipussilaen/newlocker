<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTbDetailExpress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_detail_express'))) {
            Schema::create('tb_detail_express', function (Blueprint $table) {
                $table->increments('id');
                $table->string('parcel_id');
                $table->string('no_identification');
                $table->tinyInteger('is_passport');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
