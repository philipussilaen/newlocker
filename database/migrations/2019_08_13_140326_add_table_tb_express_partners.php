<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTbExpressPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_express_partners'))) {
            Schema::create('tb_express_partners', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_express')->nullable();
                $table->string('express_number')->nullable();
                $table->string('logistics_company')->nullable();
                $table->tinyInteger('is_inbound_sent')->nullable();
                $table->tinyInteger('is_outbound_sent')->nullable();
                $table->tinyInteger('is_overdue_sent')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
